<?php
/**
 * CSV Parsing Model
 *
 * Author: Tom Procter
 */

namespace Classes;

use Classes\Config;

class CSV
{
    /**
     * @var array
     */
    private $csv;

    /**
     * @var
     */
    private $config;

    /**
     * CSV constructor.
     */
    public function __construct()
    {
        $config = new Config();
        $this->config = $config->getConfig();
        $this->csv = $this->getCsvFile();
    }

    /**
     * Load the CSV file
     *
     * @return array
     */
    private function getCsvFile()
    {
        $csv = array_map('str_getcsv', file($this->config['file']));
        return $csv;
    }

    /**
     * Get the first item from the data array to process
     * as the column headers in the database table
     *
     * @return bool|mixed
     */
    public function getCsvHeaders()
    {
        if (isset($this->csv[0])) {
            return $this->csv[0];
        }
        return false;
    }

    /**
     * Return the product data from the CSV array
     *
     * @return mixed
     */
    public function getData()
    {
        $csv = (array)$this->csv;
        $data = array_shift($csv);
        array_shift($data);
        return $csv;
    }

    /**
     * Return headers as they are in the database
     *
     * @return array
     */
    public function getColumns()
    {
        static $outputHeaders;

        $headers = $this->getCsvHeaders();
        foreach ($headers as $header) {
            $outputHeaders[] = str_replace(' ', '_', $header);
        }
        return $outputHeaders;
    }
}
