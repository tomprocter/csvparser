<?php
/**
 * Load project configuration settings
 *
 * Author: Tom Procter
 */
namespace Classes;

final class Config
{

    /**
     * Get the database configuration settings from
     * local config.ini file
     *
     * @return array|bool
     */
    public function getConfig()
    {
        $config = parse_ini_file('config.ini');
        return $config;
    }
}
