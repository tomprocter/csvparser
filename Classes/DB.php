<?php
/**
 * DB Connection Model
 *
 * Author: Tom Procter
 */
namespace Classes;

use Classes\Config;
use Classes\Debug;

class DB
{
    /**
     * @var array|bool
     */
    private $config;

    /**
     * @var \mysqli|string
     */
    public $db;

    /**
     * @var \Models\Debug
     */
    private $debug;

    /**
     * @var mixed
     */
    private $host;

    /**
     * @var mixed
     */
    private $username;

    /**
     * @var mixed
     */
    private $password;

    /**
     * @var mixed
     */
    private $dbname;

    /**
     * @var mixed
     */
    private $table;

    /**\
     * DB constructor.
     */
    public function __construct()
    {
        $config = new Config();
        $this->config = $config->getConfig();

        $this->host = $this->config['host'];
        $this->username = $this->config['username'];
        $this->password = $this->config['password'];
        $this->dbname = $this->config['dbname'];
        $this->table = $this->config['table'];

        $this->debug = new Debug();
        $this->db = $this->connectToDB();
    }


    /**
     * Connect to the MySQL database
     *
     * @return \mysqli|string
     */
    private function connectToDB()
    {
        static $connection;

        if (!isset($connection)) {
            $connection = mysqli_connect(
                $this->host,
                $this->username,
                $this->password
            );
        }

        if ($connection === false) {
            return mysqli_connect_error();
        } else {
            mysqli_set_charset($connection, "utf8");
        }

        return $connection;
    }

    /**
     * Close database connect when tasks are complete
     */
    public function closeConnection()
    {
        if ($this->db) {
            mysqli_close($this->db);
        }
    }

    /**
     * Perform a database query
     *
     * @param $query
     * @return bool|\mysqli_result
     */
    public function query($query)
    {
        $result = mysqli_query($this->db, $query) or $this->debug->error(mysqli_error($this->db));
        return $result;
    }

    /**
     * Drop Database
     *
     * @param $db
     * @return bool|\mysqli_result
     */
    public function dropDb($db)
    {
        $result = $this->query("DROP DATABASE " . $db . "");
        if ($result) {
            $this->debug->success('Database dropped.');
        }
        return $result;
    }

    /**
     * Make an existing column unique
     *
     * @param $column
     */
    public function makeUnique($column)
    {
        $this->query('ALTER TABLE ' . $this->dbname . '.' . $this->table . ' ADD UNIQUE (' . $column . ');');
    }

    /**
     * Create a Database
     *
     * @param $name
     * @return bool|\mysqli_result
     */
    public function createDatabase($name)
    {
        $result = $this->db->query("CREATE DATABASE IF NOT EXISTS " . $name . " 
            CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;");
        if ($result) {
            $this->debug->success('Database created.');
        }
        return $result;
    }

    /**
     * Create Table in the Database
     *
     * @param $name
     * @return bool|\mysqli_result
     */
    public function createTable($name)
    {
        $result = $this->query("CREATE TABLE IF NOT EXISTS " . $this->dbname . "." . $name . " (
            ID int NOT NULL AUTO_INCREMENT,
            PRIMARY KEY (ID)
            );");
        if ($result) {
            $this->debug->success('Table created.');
        }
        return $result;
    }

    /**
     * Parse headers into columns in the database
     *
     * @param $headers
     * @param $table
     * @return array
     */
    public function createColumns($headers, $table)
    {
        $i = 0;
        static $columns;

        foreach ($headers as $header) {
            $column = str_replace(' ', '_', $header);
            $columns[] = $column;
            $result = $this->query("ALTER TABLE " . $this->config['dbname'] . "." . $table . " 
                ADD COLUMN " . $column . " VARCHAR(30);");
            if ($result) {
                $this->debug->success('Column created.');
            }
            $i++;
        }

        return $columns;
    }

    /**
     * Insert the data into the database
     * @param $data
     * @param $columns
     */
    public function insertData($data, $columns)
    {
        static $headers;
        $i = 0;
        foreach ($columns as $column) {
            if ($i === 0) {
                $headers = $column;
            } else {
                $headers .= ", " . $column;
            }
            $i++;
        }

        foreach ($data as $row) {
            $values = "";
            for ($i = 0; $i <= (count($columns) - 1); $i++) {
                $inputData = $this->db->real_escape_string($row[$i]);
                if ($i === 0) {
                    $values = "'" . $inputData . "'";
                } else {
                    $values .= ", '" . $inputData . "'";
                }
            }

            $query = "INSERT INTO " . $this->dbname . "." . $this->table . " (" . $headers . ") 
                      VALUES (" . $values . ") ON DUPLICATE KEY UPDATE id=id;";
            $result = $this->query($query);
            if ($result) {
                $this->debug->success('Row inserted or updated.');
            }
        }
    }
}
