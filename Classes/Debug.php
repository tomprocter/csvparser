<?php
/**
 * Debug Model
 *
 * Author: Tom Procter
 */
namespace Classes;

class Debug
{
    public function dump($data)
    {
        if ($_SERVER['DOCUMENT_ROOT'] !== "") {
            print "<pre>";
            print_r($data);
            print "</pre>";
        }
    }

    /**
     * Display an error message in terminal
     *
     * @param $error
     */
    public function error($error)
    {
        if ($_SERVER['DOCUMENT_ROOT'] === "") {
            print "\033[31m Error:\033[0m " . $error . " \n";
        }
    }

    /**
     * Display a success message in terminal
     *
     * @param $success
     */
    public function success($success)
    {
        if ($_SERVER['DOCUMENT_ROOT'] === "") {
            print "\033[32m Success:\033[0m " . $success . " \n";
        }
    }

    /**
     * Display a generic message
     *
     * @param $message
     */
    public function message($message)
    {
        if ($_SERVER['DOCUMENT_ROOT'] === "") {
            print "\033[33m Message:\033[0m " . $message . " \n";
        }
    }
}
