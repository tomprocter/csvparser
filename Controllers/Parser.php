<?php
/**
 * DB Connection Model
 *
 * Author: Tom Procter
 */
namespace Controllers;

use Classes\Config;
use Classes\Debug;
use Classes\CSV;
use Classes\DB;

class Parser
{
    /**
     * @var CSV
     */
    private $csv;

    /**
     * @var DB
     */
    private $db;

    /**
     * @var Debug
     */
    private $debug;

    /**
     * @var array|bool
     */
    private $config;

    /**
     * @var mixed
     */
    private $dbname;

    /**
     * @var mixed
     */
    private $table;

    /**
     * Parser constructor.
     */
    public function __construct()
    {
        $this->db = new DB();
        $this->csv = new CSV();
        $this->debug = new Debug();

        $config = new Config();
        $this->config = $config->getConfig();

        $this->dbname = $this->config['dbname'];
        $this->table = $this->config['table'];
    }

    /**
     * Execute the CSV parser
     */
    public function run()
    {
        //$this->db->dropDb($this->dbname);
        $this->db->createDatabase($this->dbname);
        $this->db->createTable($this->table);
        $this->db->createColumns($this->csv->getCsvHeaders(), $this->table);
        $this->db->makeUnique('Product_Code');
        $this->db->insertData($this->csv->getData(), $this->csv->getColumns());
        $this->db->closeConnection();
    }
}
