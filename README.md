Clone the repo, set config file up, and run the index.php file to quickly import your CSV into a SQL database

# Configuration
Set the database details and the CSV file name in the config.ini file (see below for example)

# Configuration File Example:

[database]

host = localhost

username = root

password = ****

dbname = my_database

table = my_table



[csv]

file = my_file.csv

# Usage: 
git clone https://bitbucket.org/tomprocter/csvparser.git 

# Run: 
./php index.php



